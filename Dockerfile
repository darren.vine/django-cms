FROM python:3.7
MAINTAINER darren <darren@googlemail.com>

WORKDIR /app

ARG pip_access
ARG project_id
ARG project_name

RUN pip install --extra-index-url https://__token__:$pip_access@gitlab.com/api/v4/projects/$project_id/packages/pypi/simple $project_name
RUN django-admin.py startproject myproject .
COPY testapp/settings.py myproject/setting.py
#COPY testapp/urls.py myproject/urls.py

# Possible DB migrate HERE also on future updates (depending on db deployment scenarion, deployment strategy etc).  Interview discussion.
RUN python manage.py migrate

#python manage.py createsuperuser
RUN python manage.py shell -c "from django.contrib.auth.models import User; User.objects.create_superuser('admin', 'admin@example.com', 'adminpass')"

# Port 80 and other common ports are in use on my machine so i will use a random one
EXPOSE 5800 	
	
# Run 
CMD ["python", "manage.py", "runserver", "0.0.0.0:5800"]